### Please read!

This is a fork of the Introduction theme where I've hard coded some of my personal information. You probably don't want to use it. The original version is available on [GitHub](https://github.com/victoriadrake/hugo-theme-introduction), and should be used to ensure you have a proper template for your site along with the latest updates.

### Summary of changes

- Add copyright statement to footer ([commit 89a2ce70c1](https://codeberg.org/andcscott/hugo-theme-introduction/commit/89a2ce70c18b0557dc91c940976b2a65fe5c2d6d))
- Hard code mailto link in contact section ([commit 3052813dd2](https://codeberg.org/andcscott/hugo-theme-introduction/commit/3052813dd24cd4586a6938f6180146dc70861818))
- Use [Cusdis](https://cusdis.com) for comments instead of Disqus ([commit c952e080e9](https://codeberg.org/andcscott/hugo-theme-introduction/commit/c952e080e942f7e36e07ee204e1a464c5e94e043))

### License

Licensed under the [Apache License, Version 2.0](https://github.com/victoriadrake/hugo-theme-introduction/blob/master/LICENSE) (the "License"); you may not use this file except in compliance with the License. You may obtain a copy of the License at [http://www.apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0).

Unless required by applicable law or agreed to in writing, software distributed under the License is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the License for the specific language governing permissions and limitations under the License.
